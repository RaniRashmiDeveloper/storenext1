package com.rashminidhi.storenext1;

import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Locale;

import static com.rashminidhi.storenext1.R.id.imageView;
import static com.rashminidhi.storenext1.R.id.textView;

/**
 * Created by RASHMI on 11/15/2016.
 */

public class NaviagtionListAdapter extends BaseAdapter {

    private String[] itemsName;
    private int [] imagesid;
    Context context;
    private static LayoutInflater inflater=null;


    public NaviagtionListAdapter(Context context, String[] itemsName, int[] imagesid) {

            this.itemsName = itemsName;
            this.imagesid = imagesid;
            this.context = context;
        inflater = ( LayoutInflater )context.
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return itemsName.length;
    }

    @Override
    public Object getItem(int i) {
        return itemsName[i];
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    public class Holder
    {
        TextView tv;
        ImageView img;
    }
    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        Holder holder = new Holder();

        View v = inflater.inflate(R.layout.list_row,null);
        holder.tv = (TextView) v.findViewById(textView);
        holder.img= (ImageView) v.findViewById(imageView);

        AssetManager am = context.getApplicationContext().getAssets();

        Typeface typeface = Typeface.createFromAsset(am,
                String.format(Locale.US, "fonts/%s", "fonts.ttf"));
        holder.tv.setTypeface(typeface);

        holder.tv.setText(itemsName[i]);
        holder.img.setImageResource(imagesid[i]);

        return v;
    }
}
