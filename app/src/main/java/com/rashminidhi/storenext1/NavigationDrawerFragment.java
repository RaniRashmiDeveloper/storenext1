package com.rashminidhi.storenext1;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;


/**
 * A simple {@link Fragment} subclass.
 */
public class NavigationDrawerFragment extends Fragment {

    public Context mContext;
    public ActionBarDrawerToggle mDrawerToggle;
    public View containerView;
    public DrawerLayout mDrawerLayout;
    public ListView navigationListView;
    public NaviagtionListAdapter navigationListAdapter;
    public String [] items = {"Home","Offers","Deals","Profile"};
    public static int [] images={R.drawable.home,R.drawable.offers,R.drawable.deals,R.drawable.profile};



    public NavigationDrawerFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v =  inflater.inflate(R.layout.fragment_navigation_drawer, container, false);
        navigationListView = (ListView)v.findViewById(R.id.listView);
        navigationListView.setAdapter(new NaviagtionListAdapter(getActivity(),items,images));

        return v;
    }

    public void setFragment(int navigationDrawerFragment, DrawerLayout viewById, Toolbar toolbar) {
        containerView=getActivity().findViewById(navigationDrawerFragment);
        mDrawerLayout= viewById;
        mDrawerToggle=new ActionBarDrawerToggle(getActivity(),mDrawerLayout,toolbar,R.string.navigation_drawer_close,R.string.navigation_drawer_close){
            @Override
            public void onDrawerOpened(View drawerView)
            {
                super.onDrawerOpened(drawerView);
                getActivity().invalidateOptionsMenu();
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                getActivity().invalidateOptionsMenu();
            }
        };
        mDrawerLayout.addDrawerListener(mDrawerToggle);
    }
}
